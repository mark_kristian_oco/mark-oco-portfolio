import "./styles.scss";

const About = () => {
  return (
    <div id="about" className="pageContainer">
      <div className="titleContainer">
        <span className="pageTitle">About Me</span>
        <div className="divider" />
      </div>
      <p className="description">
        I'm Mark Kristian Oco, but most people who know me call me by my
        nickname <span className="nickname">Potch</span>. I am a highly driven
        and creative web developer with knowledge in designing and developing
        robust websites and mobile applications.
      </p>
    </div>
  );
};

export default About;
