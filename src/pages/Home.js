import "./styles.scss";

const Home = () => {
  return (
    <div className="container">
      <span className="subtitle">I'm</span>
      <span className="name">Potch,</span>
      <span className="title">Just Your</span>
      <span className="title">Average</span>
      <span className="title">Full Stack</span>
      <span className="title">Developer</span>
    </div>
  );
};

export default Home;
