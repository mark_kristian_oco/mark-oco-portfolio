import React from "react";
import Project from "../components/Project";
import "./styles.scss";

const prjoects = [
  {
    title: "EMMA by AXA",
    description:
      "Powered by AXA Philippines, Emma by AXA PH is your dedicated partner in your journey towards financial wellness. Access new features, including AXA Rescue Line, your access to 24/7 emergency assistance, and buy insurance online.",
    stacks: ["ReactJ Native Cli", "Redux Saga"],
    image: "/images/emmamobile.png",
    links: [
      {
        label: "Go to App Store",
        uri: "https://apps.apple.com/ph/app/emma-by-axa-ph/id1483188126",
      },
      {
        label: "Go to Playstore",
        uri: "https://play.google.com/store/apps/details?id=com.axa.ph.emma",
      },
    ],
  },
  {
    title: "AXA Healthstartlite",
    description:
      "AXA Philippines is one of the largest and fastest growing insurance companies in the country, offering financial security to 1.3 million individuals through our group and individual life insurance products.",
    stacks: ["ReactJs", "Styled Component", "Redux"],
    image: "/images/axaweb.png",
    links: [
      {
        label: "Visit website",
        uri: "https://www.axa.com.ph/healthstartlite/",
      },
    ],
  },
  {
    title: "Media Meter (Mobile App)",
    description:
      "The new and improved Media Meter app. Its features include the on-time reading of related articles, easier access to your account, and notifying users of new articles through push notifications.",
    stacks: ["React Native", "Context API", "Expo", "Android", "IOS"],
    image: "/images/v3mobile.png",
    links: [
      {
        label: "Go to App Store",
        uri: "https://apps.apple.com/us/app/media-meter-v3/id1563889019",
      },
      {
        label: "Go to Playstore",
        uri: "https://play.google.com/store/apps/details?id=com.mmi.mediameterv3",
      },
    ],
  },
  {
    title: "Media Meter (Web App)",
    description:
      "Media Meter helps organizations and companies track PR campaigns including hundreds of newspapers, magazines, radio and TV stations, and blogs every day so they don’t have to.",
    stacks: ["ReactJs", "Material UI", "Redux", "Web"],
    image: "/images/v3web1.png",
    links: [
      { label: "Go to Website", uri: "https://media-meter.net/mm-client/" },
      {
        label: "Watch Demo",
        uri: "https://www.youtube.com/watch?v=fs2HneOJDdI",
      },
    ],
  },
  {
    title: "Media Meter (Marketing Website)",
    description:
      "Media Meter offers a range of plans that will help you make best use of your media monitoring and analytics budget.",
    stacks: ["NextJs", "Ant Design", "Lottie"],
    image: "/images/mmimarketing.png",
    links: [{ label: "Visit Website", uri: "https://media-meter.net/" }],
  },
];

const Projects = () => {
  return (
    <div id="projects" className="pageContainer">
      <div className="titleContainer">
        <span className="pageTitle">Featured Projects</span>
        <div className="divider" />
      </div>
      <div className="projContainer">
          {prjoects.map((project, index) => (
            <Project key={index} project={project} />
          ))}
        </div>
    </div>
  );
};

export default React.memo(Projects);
