import React from "react";

const Skills = () => {
  return (
    <div id="skills" className="pageContainer">
      <div className="titleContainer">
        <span className="pageTitle">Skills</span>
        <div className="divider" />
      </div>

      <div className="skillsContainer">
        <div>
          <div>
            <span>Full Stack Development</span>
          </div>
          <img
            alt="html"
            title="HTML"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/html.png"}
            height="45px"
          />
          <img
            alt="css"
            title="CSS"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/css.png"}
            height="45px"
          />
          <img
            alt="javascript"
            title="Javascript"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/javascript.png"}
            height="45px"
          />
          <img
            alt="Bootstrap"
            title="Bootstrap"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/bootstrap.png"}
            height="45px"
          />
          <img
            alt="PHP"
            title="PHP"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/php.png"}
            height="40px"
          />
          <img
            alt="MySQL"
            title="MySQL"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/mysql.webp"}
            height="45px"
          />
          <img
            alt="Laravel"
            title="Laravel"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/laravel.png"}
            height="45px"
          />
          <img
            alt="MongoDbL"
            title="MongoDb"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/mongodb.png"}
            height="45px"
          />
          <img
            alt="ExpressJs"
            title="ExpressJs"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/express.png"}
            height="45px"
          />
          <img
            alt="ReactJs"
            title="ReactJs"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/reactjs.webp"}
            height="45px"
          />
          <img
            alt="NodeJs"
            title="NodeJs"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/node.png"}
            height="45px"
          />
          <img
            alt="Postman"
            title="Postman"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/postman.png"}
            height="45px"
          />
          <img
            alt="Gitlab"
            title="Gitlab"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/gitlab.webp"}
            height="45px"
          />
          <img
            alt="Linux"
            title="Linux"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/linux.png"}
            height="40px"
          />
        </div>
      </div>
      <div className="skillsContainer">
        <div>
          <div>
            <span>Data Analysis</span>
          </div>
          <img
            alt="Google Docs"
            title="Google Docs"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/docs.png"}
            height="45px"
          />
          <img
            alt="Google Spreadsheets"
            title="Google Spreadsheets"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/sheets.webp"}
            height="45px"
          />
          <img
            alt="Google Slides"
            title="Google Slides"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/slide.png"}
            height="45px"
          />
          <img
            alt="Data Studio"
            title="Data Studio"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/datastudio.png"}
            height="45px"
          />
          <img
            alt="Google Apps Script"
            title="Google Apps Script"
            className="m-3"
            src={process.env.PUBLIC_URL + "/images/gas.png"}
            height="45px"
          />
        </div>
      </div>
    </div>
  );
};

export default Skills;
