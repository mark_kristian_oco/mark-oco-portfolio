import { useRef } from "react";
import "./styles.scss";

const NavItem = (props) => {
  const {
    label,
    setElPosition,
    setNavHovered,
    route,
  } = props;

  const navRef = useRef(null);

  if (navRef.current !== null) {
    navRef.current.onmouseover = () => {
      setNavHovered(true);
      setElPosition(navRef.current.getBoundingClientRect());
    };
    navRef.current.onmouseout = () => {
      setNavHovered(false);
    };
  }

  

  return (
    <span ref={navRef}>
      <a className={"nav"} href={route} target={label === "Hire Me" ? "_blank" : ""} rel="noreferrer" >
        {label}
      </a>
    </span>
  );
};

export default NavItem;
