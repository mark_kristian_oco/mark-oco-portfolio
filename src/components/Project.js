import React, { useRef, useEffect, useState } from "react";

const Project = ({ project }) => {
  const projref = useRef(null);
  const [isHovered, setIsHovered] = useState(false);
  useEffect(() => {
    if (projref.current !== null) {
      projref.current.onmouseover = () => {
        setIsHovered(true);
      };
      projref.current.onmouseout = () => {
        setIsHovered(false);
      };
    }
  }, []);

  return (
    <div className="projContainer">
      <div
        ref={projref}
        className="imgContainer"
        onClick={() => window.open(project.links[0]?.uri)}
      >
        <img
          alt={project.title}
          className={isHovered ? "imgHovered" : "img"}
          src={process.env.PUBLIC_URL + project.image}
        />
      </div>
      <div className="projDetails">
        <span className="projTitle">{project.title}</span>
        <p className="projDescription">{project.description}</p>
        <div>
          {project.stacks.map((stack, i) => (
            <span key={i} className="chips">
              {stack}
            </span>
          ))}
        </div>
        <div className="btnContainer">
          {project.links.map((link, i) => (
            <a href={link.uri} target="_blank"  rel="noreferrer" key={i} className="btn">
              {link.label}
            </a>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Project;
