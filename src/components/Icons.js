import { useRef, useState } from "react";
import FontAwesome from "react-fontawesome";
import "./styles.scss";

const Icons = ({ name, tooltipLabel, setLinkHovered, setElPosition, onClick }) => {
  const linkref = useRef(null);
  const [tooltipVisible, setTooltipVisible] = useState(false);

  if (linkref.current !== null) {
    linkref.current.onmouseover = () => {
      setLinkHovered(true);
      setElPosition(linkref.current.getBoundingClientRect());
    };
    linkref.current.onmouseout = () => {
      setLinkHovered(false);
      setElPosition(null);
    };
  }

  return (
    <div ref={linkref} className="iconContainer">
      <FontAwesome
        onClick={onClick}
        onMouseOver={() => setTooltipVisible(true)}
        onMouseOut={() => setTooltipVisible(false)}
        name={name}
        size="lg"
        className="icon iconGreen"
      />
      {tooltipVisible && tooltipLabel && (
        <span className="tooltip">{tooltipLabel}</span>
      )}
    </div>
  );
};

export default Icons;
