import "./styles.scss";

const Backdrop = ({ children, showSidebar, setShowSidebar }) => {
  return (
    <div
      onClick={() => setShowSidebar(false)}
      className={showSidebar ? "backdrop" : "backdropHidden"}
    >
      {children}
    </div>
  );
};

export default Backdrop;
