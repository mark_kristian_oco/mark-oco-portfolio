import { useRef } from "react";
import FontAwesome from "react-fontawesome";
import Icons from "./Icons";

const Header = ({ setLinkHovered, setShowSidebar, setElPosition }) => {

  const linkref = useRef(null)
  if(linkref.current !== null){
    linkref.current.onmouseover = () => {
      setElPosition(linkref.current.getBoundingClientRect())
      setLinkHovered(true)
    };
    linkref.current.onmouseout = () => {
      setLinkHovered(false)
      setElPosition(null)
    };
  }

  return (
    <div id="home" className="header">
      <div className="flexRow">
        <span className="intial">MKVO</span>
        <Icons
          setElPosition={setElPosition}
          setLinkHovered={setLinkHovered}
          name="gitlab"
          tooltipLabel="Gitlab"
          onClick={() => window.open("https://gitlab.com/mark_kristian_oco")}
        />
        <Icons
          setElPosition={setElPosition}
          setLinkHovered={setLinkHovered}
          name="linkedin"
          tooltipLabel="Linkedin"
          onClick={() => window.open("https://www.linkedin.com/in/mark-kristian-oco-a784a4110/")}
        />
        <Icons
          setElPosition={setElPosition}
          setLinkHovered={setLinkHovered}
          name="envelope"
          tooltipLabel="Gmail"
          onClick={() => window.open("https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=markoco.dev@gmail.com")}
        />
      </div>

      <div onClick={() => setShowSidebar(true) } ref={linkref} className="iconContainer">
        <FontAwesome
          name="bars"
          size="lg"
          className="icon"
        />
      </div>
     
    </div>
  );
};

export default Header;
