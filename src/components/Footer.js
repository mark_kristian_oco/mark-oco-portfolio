import React from 'react'
import "./styles.scss";

function Footer() {
  return (
    <div className="footer">
      © 2021 Mark Kristian Oco. All rights reserved.
    </div>
  )
}

export default Footer
