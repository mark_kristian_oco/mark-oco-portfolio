import { useRef } from "react";
import NavItem from "./NavItem";
import "./styles.scss";

const Sidebar = ({
  showSidebar,
  setShowSidebar,
  setNavHovered,
  setElPosition
}) => {


  const navRef = useRef(null);
  if (navRef.current !== null) {
    navRef.current.onmouseover = () => {
      setElPosition(null);
      setNavHovered(true);
      setElPosition(navRef.current.getBoundingClientRect());
    };
    navRef.current.onmouseout = () => {
      setNavHovered(false);
      setElPosition(null);
    };
  }

  const getSidebarStyle = showSidebar ? "sidebar" : "sidebar sidebarHidden";

  return (
    <div className={getSidebarStyle}>
      <div className="spacer">
        <div className="divider" />
        
        <span className="caption1">An entire section dedicated to me</span>
        <div className="caption2">
          Need I say more? Just see for youself... Come on don't be shy
        </div>
        <span ref={navRef} className="caption3">
          <a className="link" target="_blank" rel="noreferrer" href="https://bit.ly/3tNKvho">
            View My Resume
          </a>
        </span>

        <div className="divider" />
        <NavItem
          setShowSidebar={setShowSidebar}
          setElPosition={setElPosition}
          setNavHovered={setNavHovered}
          label="Home"
          route="#home"
        />
        <NavItem
          setShowSidebar={setShowSidebar}
          setElPosition={setElPosition}
          setNavHovered={setNavHovered}
          label="Projects"
          route="#projects"
        />
         <NavItem
          setShowSidebar={setShowSidebar}
          setElPosition={setElPosition}
          setNavHovered={setNavHovered}
          label="About"
          route="#about"
        />
        <NavItem
          setShowSidebar={setShowSidebar}
          setElPosition={setElPosition}
          setNavHovered={setNavHovered}
          label="Skills"
          route="#skills"
        />
        <NavItem
          setShowSidebar={setShowSidebar}
          setElPosition={setElPosition}
          setNavHovered={setNavHovered}
          label="Hire Me"
          route="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=markoco.dev@gmail.com"
        />
      </div>
    </div>
  );
};

export default Sidebar

