import React, { useEffect, useRef, useState } from "react";
import Header from "./components/Header";
import Backdrop from "./components/Backdrop";
import { colors } from "./theme";
import Sidebar from "./components/Sidebar";
import FontAwesome from "react-fontawesome";
import "./index.scss";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import Projects from "./pages/Projects";
import Skills from "./pages/Skills";
import About from "./pages/About";

function App() {
  const [linkHovered, setLinkHovered] = useState(false);
  const [navHovered, setNavHovered] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);
  const [elPosition, setElPosition] = useState({});
  const [isCursorHidden, setIsCursorHidden] = useState(false);

  const cursorRef = useRef(null);
  // const cursorDotRef = useRef(null);

  useEffect(() => {
    if (showSidebar) {
      unloadScrollBars();
    } else {
      reloadScrollBars();
    }
  }, [showSidebar]);

  function reloadScrollBars() {
    document.documentElement.style.overflowY = "auto"; // firefox, chrome
    document.body.scroll = "yes"; // ie only
  }

  function unloadScrollBars() {
    document.documentElement.style.overflowY = "hidden"; // firefox, chrome
    document.body.scroll = "no"; // ie only
  }

  const cursor = (e) => {
    if (isCursorHidden) return;

    if (cursorRef.current === null) return;

    const cursor = cursorRef.current;
    // const cursorDot = cursorDotRef.current;

    let y = e.pageY + "px";
    let x = e.pageX + "px";

    if (elPosition) {
      // console.log(projHovered);
    }

    if (linkHovered && elPosition) {
      y = (elPosition.top + elPosition.bottom) / 2 + "px";
      x = (elPosition.left + elPosition.right) / 2 + "px";
    }

    //Set cursor positon when sidebar navitem is hovered
    if (navHovered && elPosition) {
      y = (elPosition.top + elPosition.bottom) / 2 + "px";
      x = elPosition.left - 40 + "px";
    }

    cursor.style.top = y;
    cursor.style.left = x;
  };

  window.addEventListener("mousemove", cursor);

  // Hide custom cursor when dev tool is open to avoid lag.
  window.onresize = function () {
    if (window.outerWidth - window.innerWidth > 100) {
      setIsCursorHidden(true);
      document.body.style.cursor = "pointer";
    } else {
      setIsCursorHidden(false);
      document.body.style.cursor = "none";
    }
  };

  useEffect(() => {
    if (window.innerWidth < 900) {
      setIsCursorHidden(true);
    }
  }, []);

  const getCursorStyle = linkHovered
    ? "cursorGrow"
    : navHovered
    ? "cursorGrowNav"
    : "cursor";

  return (
    <>
      <Backdrop showSidebar={showSidebar} setShowSidebar={setShowSidebar}>
        <Sidebar
          setNavHovered={setNavHovered}
          setElPosition={setElPosition}
          showSidebar={showSidebar}
          setShowSidebar={setShowSidebar}
        />
      </Backdrop>

      <Header
        setElPosition={setElPosition}
        setShowSidebar={setShowSidebar}
        setLinkHovered={setLinkHovered}
      />
      <Home />
      <Projects />
      <About />
      <Skills />
      <Footer />

      {/* Custom mouse */}
      {!isCursorHidden && (
        <>
          {" "}
          <div ref={cursorRef} className={getCursorStyle}>
            <FontAwesome
              className={navHovered ? "arrow" : "arrowHidden"}
              name="chevron-right"
              color={colors.black}
            />
          </div>
          {/* <div
            ref={cursorDotRef}
            className={
              linkHovered || navHovered ? "cursorDotHidden" : "cursorDot"
            }
          />{" "} */}
        </>
      )}
    </>
  );
}

export default App;
